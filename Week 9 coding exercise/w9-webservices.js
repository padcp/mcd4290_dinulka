function geoFindMe(){

    const status = document.querySelector('#status');

    function success(position){
        let latitude  = position.coords.latitude;
        let longitude = position.coords.longitude;
        mapIntegration(longitude,latitude)
        APIservice(mapIntegration(longitude,latitude))
    }

    function error() {
        status.textContent = 'Unable to retrieve your location';
    }

    if(!navigator.geolocation) {
        status.textContent = 'Geolocation is not supported by your browser';
    } else {
        status.textContent = 'Locating…';
        navigator.geolocation.getCurrentPosition(success, error);
    }
    
    document.getElementById("find-me").remove()
    document.getElementById("status").remove()
}

function mapIntegration(longitude,latitude){
    mapboxgl.accessToken = 'pk.eyJ1IjoibmF2aW5kdTU1IiwiYSI6ImNsMmtlczJ2ajE2NzczYnA5MWZyZjQwZGQifQ.XQ5-1kiL0oFyOlDE63VHgg';
    
    let yourLocation = [longitude, latitude];

    let map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10',
        zoom: 16,
        center: yourLocation
    });

    let marker = new mapboxgl.Marker().setLngLat(yourLocation).addTo(map).setPopup(new mapboxgl.Popup().setText("Your location").addTo(map));
    
    return map
}

function APIservice(map){
    let bounds = map.getBounds()
    let token = "e5c69fd97a084aa067bbcc93fefdc3036bce07dc";
    let latlng = `${bounds._sw.lat},${bounds._sw.lng},${bounds._ne.lat},${bounds._ne.lng}`;
    let url = "https://api.waqi.info/map/bounds";
    let webservice = url + "/?token=" + encodeURIComponent(token) + "&latlng=" + latlng + "&callback=dataFunction";
    console.log(webservice)
    let script = document.createElement('script');
    script.src = webservice;
    document.body.appendChild(script);

    function dataFunction(results){
        console.log(results)
    }
}