//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "Question1"; //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
	
	//Given array of numbers
	let Given = [54,-16,80,55,-74,73,26,5,-34,-73,19,63,-55,-61,65,-14,-19,-51,-17,-25];
	
	let pos_odd = [];
	let neg_even = [];
	
	//Allocating data to relevant arrays
	for (i of Given)
	{
		if (i>0)
		{
			if (i % 2 !==0)
				pos_odd.push(i)
		}
		else
		{
			if (i % 2 ===0)
				neg_even.push(i)
		}

	}
		output = "Question 1\n\n" + "Positive Odd: " + pos_odd + "\nNegative Even: " + neg_even //empty output, fill this so that it can print onto the page.
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here
	
	//Declaring outcomes as variables
	let one = 0;
	let two = 0;
	let three = 0;
	let four = 0;
	let five = 0;
	let six = 0;
	
	//Initiating for loop for 60000 die rolls
	for (i =0; i<60000; i++)
	{
		let dice = Math.floor((Math.random() * 6) + 1);
		
		if (dice === 1)
		{
			one += 1;
		}
		else if (dice ===2)
		{
			two += 1;
		}
		else if (dice === 3)
		{
			three += 1;
		}
		else if (dice === 4)
		{
			four += 1;
		}
		else if (dice === 5)
		{
			five += 1;
		}
		else if (dice === 6)
		{
			six += 1;
		}
	}
	
    //Displaying the ouput
	
	output = "Question 2\n\n" +
			"Frequency of die rolls\n" +
			"1 : " + one + "\n" +
			"2 : " + two + "\n" +
			"3 : " + three + "\n" +
			"4 : " + four + "\n" +
			"5 : " + five + "\n" +
			"6 : " + six 
			
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
   //Question 3 here
let numDice = [0,0,0,0,0,0,0];
for (var i=1; i<60000; i++ ){
const randNum = Math.floor((Math.random() * 6) + 1);
numDice[randNum] += 1;
}
	output =  "Question 3\n\n" +
			"Frequency of die rolls\n" +
			"1 : " + numDice[1] + "\n" +
			"2 : " + numDice[2] + "\n" +
			"3 : " + numDice[3] + "\n" +
			"4 : " + numDice[4] + "\n" +
			"5 : " + numDice[5] + "\n" +
			"6 : " + numDice[6] 	
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here
	var dieRolls = {
            Frequencies: {
                    1:0,
                    2:0,
                    3:0,
                    4:0,
                    5:0,
                    6:0,
                },
            Total:60000,
            Exceptions: ""
}
for (var i=1; i<dieRolls.Total; i++ ){
  const randNum = Math.floor((Math.random() * 6) + 1);
  dieRolls.Frequencies[randNum] += 1;
}
//Determining die faces that are excepetions
for (var i=1; i<7; i++){
  if (dieRolls.Frequencies[i]>10100 || dieRolls.Frequencies[i]<9900) 
    {
        dieRolls.Exceptions += i + " ";
    }
}
//Displaying the output
output = "Question 4\n\n" + 
		"Frequency of dice rolls" + "\n" +
		"Total Rolls: " + dieRolls.Total + "\n" +
		"Frequencies:" + "\n" 
		
		//Displaying the frequencies using for in
		for (let propNum in dieRolls.Frequencies)
		{
			output += propNum + ": " + dieRolls.Frequencies[propNum] + "\n";

		}

//Displaying exceptions to output
	output += "Exceptions: " + dieRolls.Exceptions;
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 

//Creating object with relevent infomration	
	let person = {
			name: "Jane",
			income: 127050
		}

let tax = 0;
//Calculating the tax owed

if (person.income > 18200 && person.income <= 37000)
{
	tax = (person.income - 18200) * 0.19;
}
else if (person.income > 37000 && person.income <= 90000)
{
	tax = ((person.income - 37000) * 0.325) + 3572;
}
else if (person.income > 90000 && person.income <= 180000)
{
	tax = ((person.income - 90000) * 0.37) + 20797;
}
else if (person.income > 180000)
{
	tax = ((person.income - 180000) * 0.45) + 54097;
}
else
{
	tax = 0;
}

//Displaying output

output = "Question 5\n\n" + person.name + "'s income is: $" + person.income + ", and her tax owed is: $" + tax.toFixed(2);
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}